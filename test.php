<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 16.11.19
 * Time: 21:27
 */

include __DIR__ . "/vendor/autoload.php";

$conf = 4;

$enigma = new \src\Enigma($conf);
$str = ['a', 'b', 'c'];
$code = [];
foreach ($str as $s) {
    $code[] = $enigma->encode($s);
}
print_r($code);

$decode = [];
$enigma = new \src\Enigma($conf);
foreach ($code as $s) {
    $decode[] = $enigma->encode($s);
}
print_r($decode);
