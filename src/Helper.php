<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 16.11.19
 * Time: 11:24
 */

namespace src;


class Helper
{
    protected $rotor = [];
    protected $reflector = [];
    protected $alphabet;
    protected $count;

    /**
     * @return array
     */
    public function getRotor()
    {
        return $this->rotor;
    }

    /**
     * @return array
     */
    public function getReflector()
    {
        return $this->reflector;
    }

    /**
     * @return array
     */
    public function getAlphabet()
    {
        return $this->alphabet;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }

    public function __construct()
    {
        $this->alphabet = explode(' ', 'a b c d e f g h i j k l m n o p q r s t u v w x y z');
        $this->count = count($this->alphabet);
    }

    public function generateRotor()
    {
        $this->rotor = range(0, 25);
        shuffle($this->rotor);
    }

    public function generateReflector()
    {
        $base = range(0, 25);
        $reflector = [];
        shuffle($base);
        for ($i = 0; $i < $this->count; $i += 2) {
            $reflector[$base[$i]] = $base[$i + 1];
            $reflector[$base[$i + 1]] = $base[$i];
        }
        for ($i = 0; $i < $this->count; $i++) {
            $this->reflector[] = $reflector[$i];
        }
    }

    public function printStructure($structure)
    {
        echo "[\n";
        foreach ($structure as $k => $v) {
            echo "    $k => $v, \n";
        }
        echo "]\n";
    }
}