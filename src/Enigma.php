<?php
/**
 * http://www.codesandciphers.org.uk/enigma/rotorspec.htm
 *
 */
namespace src;

class Enigma
{
    protected $alphabet = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r",
        "s", "t", "u", "v", "w", "x", "y", "z"];

    protected $rotor = [21, 7, 1, 22, 12, 19, 11, 3, 6, 25, 16, 4, 17, 14, 5, 13, 9, 10, 23, 8, 20, 24, 2, 15, 18, 0];
    protected $reflector = [10, 25, 24, 7, 15, 20, 12, 3, 9, 8, 0, 18, 6, 14, 13, 4, 21, 19, 11, 17, 5, 16, 23, 22, 2, 1];

    protected $position = 0;
    protected $count;

    public function __construct($position = 0)
    {
        $this->count = count($this->alphabet);
        $this->rotate($position);
    }

    public function encode($letter)
    {
        //letter index
        $index = $this->shift(array_search($letter, $this->alphabet));
        //rotor front
        $index = $this->rShift($this->rotor[$index]);
        //reflector
        $index = $this->shift($this->reflector[$index]);
        //rotor back
        $index = $this->rShift(array_search($index, $this->rotor));
        $this->rotate();
        return $this->alphabet[$index];
    }

    protected function shift($index)
    {
        return ($index + $this->position) % $this->count;
    }

    protected function rShift($index)
    {
        return ($index + $this->count - $this->position) % $this->count;
    }

    protected function rotate($position = 1)
    {
        $this->position += $position;
        $this->position %= $this->count;
    }

    public function setPosition($position)
    {
        $this->position = 0;
        $this->rotate($position);
    }
}
