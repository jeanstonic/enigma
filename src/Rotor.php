<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 16.11.19
 * Time: 20:54
 */

namespace src;


class Rotor
{
    protected $rotor;
    protected $count;

    /**
     * Rotor constructor.
     * @param array $rotor
     */
    public function __construct($rotor)
    {
        $this->count = count($rotor);
        $this->rotor = $rotor;
    }

    /**
     * @param int $index
     * @return int
     */
    public function front($index)
    {
        return $this->rotor[$index];
    }

    /**
     * @param int $index
     * @return int
     */
    public function back($index)
    {
        return array_search($index, $this->rotor);
    }
}