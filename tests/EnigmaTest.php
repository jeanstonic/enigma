<?php
use PHPUnit\Framework\TestCase;
use src\Enigma;


class EnigmaTest extends TestCase
{
    private $config = 0;
    private $config2 = 2;
    private $string = ['a', 'a', 'b'];


    public function testSimpleEncode()
    {
        $enigma = new Enigma($this->config);

        $this->assertNotEquals("a", $enigma->encode("a"));
    }

    public function testEncode()
    {
        $enigma = new Enigma($this->config);

        $encoded[0] = $enigma->encode($this->string[0]);
        $encoded[1] = $enigma->encode($this->string[1]);

        $this->assertNotEquals($encoded[0], $this->string[0]);
        $this->assertNotEquals($encoded[1], $this->string[1]);
        $this->assertNotEquals($encoded[0], $encoded[1]);
    }

    public function testDecode()
    {
        $enigma = new Enigma($this->config);
        $encoded[0] = $enigma->encode($this->string[0]);
        $encoded[1] = $enigma->encode($this->string[1]);
        $encoded[2] = $enigma->encode($this->string[2]);

        $enigma = new Enigma($this->config);
        $decoded[0] = $enigma->encode($encoded[0]);
        $decoded[1] = $enigma->encode($encoded[1]);
        $decoded[2] = $enigma->encode($encoded[2]);
        $this->assertEquals($this->string, $decoded);
    }
}
