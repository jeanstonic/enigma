<?php


use PHPUnit\Framework\TestCase;
use src\Helper;

class HelperTest extends TestCase
{

    public function testAllRotorKeys()
    {
        $helper = new Helper();
        $helper->generateRotor();
        $e = range(0, $helper->getCount() - 1);
        $k = array_keys($helper->getRotor());
        $v = array_values($helper->getRotor());
        sort($v);
        $this->assertEquals($e, $k);
        $this->assertEquals($e, $v);
    }

    public function testArrayEquals()
    {
        $a = range(0, 25);
        shuffle($a);
        $v = array_values($a);
        sort($v);
        $k = range(0, 25);
        $this->assertEquals($k, $v);
    }


}
