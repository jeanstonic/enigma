<?php

include __DIR__ . '/src/Enigma.php';

echo "position: ";
$position = readline();
$enigma = new \src\Enigma($position);

echo "start typing\n\n";

$word = $code = '';
while (true) {
    echo ": ";
    $input = readline();
    if ($input == 'halt') {
        echo "\n$word -> $code\n\n";
        break;
    }
    if ($input == 'reset') {
        $enigma->setPosition($position);
        echo "\n$word -> $code\n\n";
        $word = $code = '';
        continue;

    }
    $word .= $input;
    $letter = $enigma->encode($input);
    echo " -> $letter\n";
    $code .= $letter;
}
