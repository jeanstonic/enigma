<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 16.11.19
 * Time: 22:12
 */


include __DIR__ . "/vendor/autoload.php";

$conf = [];
$helper = new \src\Helper();
$helper->generateReflector();
$helper->generateRotor();

$conf['alphabet'] = $helper->getAlphabet();
$conf['rotor'] = $helper->getRotor();
$conf['reflector'] = $helper->getReflector();

file_put_contents(__DIR__ . "/src/conf.json", json_encode($conf));